package org.luisarturopozuelos.manejadores;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import org.luisarturopozuelos.bean.TelefonoClientes;
import org.luisarturopozuelos.bean.Compras;
import org.luisarturopozuelos.bean.DetalleCompras;
import org.luisarturopozuelos.bean.DetalleFacturas;
import org.luisarturopozuelos.bean.DireccionClientes;
import org.luisarturopozuelos.bean.DireccionProveedores;
import org.luisarturopozuelos.bean.EmailClientes;
import org.luisarturopozuelos.bean.EmailProveedores;
import org.luisarturopozuelos.bean.Empaques;
import org.luisarturopozuelos.bean.Inventarios;
import org.luisarturopozuelos.bean.Stocks;
import org.luisarturopozuelos.bean.TelefonoProveedores;
import org.luisarturopozuelos.db.Conexion;

public class ManejadorTelefonoProveedores {
      private ArrayList<TelefonoProveedores> lista = new ArrayList<TelefonoProveedores>();
    public ManejadorTelefonoProveedores (){

    }
     public ArrayList<TelefonoProveedores> getLista() {
        ResultSet datos = Conexion.getInstancia().hacerConsulta("select * from vw_TelefonoProveedores");
        try{
            while(datos.next()){
               lista.add(new TelefonoProveedores (datos.getInt("idTelefono"), datos.getInt("idProveedor"), datos.getInt("idTipo"), datos.getInt("telefono"))); 
            }
            }catch(SQLException e){
            e.printStackTrace();
        }
        return lista;
    }
}
