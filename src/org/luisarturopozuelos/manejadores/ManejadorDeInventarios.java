package org.luisarturopozuelos.manejadores;
import java.util.ArrayList;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.luisarturopozuelos.bean.Compras;
import org.luisarturopozuelos.bean.Inventarios;
import org.luisarturopozuelos.bean.Facturas;
import org.luisarturopozuelos.bean.Proveedores;
import org.luisarturopozuelos.db.Conexion;

public class ManejadorDeInventarios {
    private ArrayList<Inventarios> lista = new ArrayList<Inventarios>();
    public ManejadorDeInventarios() {

    }
     public ArrayList<Inventarios> getLista() {
        ResultSet datos = Conexion.getInstancia().hacerConsulta("select * from vw_Inventario");
        try{
            while(datos.next()){
               lista.add(new Inventarios (datos.getInt("idMovimiento"), datos.getDate("fecha"),
                       datos.getInt("idProducto"), datos.getInt("idTipoMovimiento"),datos.getInt("idFactura"),
                       datos.getInt("idCompra"), datos.getInt("lineaNo"), datos.getFloat("cantidad"), datos.getFloat("precio"))); 
            }
            }catch(SQLException e){
            e.printStackTrace();
        }
        return lista;
    }
}

