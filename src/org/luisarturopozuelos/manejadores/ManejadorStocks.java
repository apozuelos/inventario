package org.luisarturopozuelos.manejadores;
import java.util.ArrayList;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.luisarturopozuelos.bean.Compras;
import org.luisarturopozuelos.bean.DetalleCompras;
import org.luisarturopozuelos.bean.DetalleFacturas;
import org.luisarturopozuelos.bean.DireccionClientes;
import org.luisarturopozuelos.bean.DireccionProveedores;
import org.luisarturopozuelos.bean.EmailClientes;
import org.luisarturopozuelos.bean.EmailProveedores;
import org.luisarturopozuelos.bean.Empaques;
import org.luisarturopozuelos.bean.Inventarios;
import org.luisarturopozuelos.bean.Stocks;
import org.luisarturopozuelos.bean.Proveedores;
import org.luisarturopozuelos.db.Conexion;
public class ManejadorStocks {
     private ArrayList<Stocks> lista = new ArrayList<Stocks>();
    public ManejadorStocks (){

    }
     public ArrayList<Stocks> getLista() {
        ResultSet datos = Conexion.getInstancia().hacerConsulta("select * from vw_Stocks");
        try{
            while(datos.next()){
               lista.add(new Stocks (datos.getInt("idStock"), datos.getFloat("stock"))); 
            }
            }catch(SQLException e){
            e.printStackTrace();
        }
        return lista;
    }
    
}
