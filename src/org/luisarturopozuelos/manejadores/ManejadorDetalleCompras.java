
package org.luisarturopozuelos.manejadores;
import java.util.ArrayList;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.luisarturopozuelos.bean.Compras;
import org.luisarturopozuelos.bean.Inventarios;
import org.luisarturopozuelos.bean.DetalleCompras;
import org.luisarturopozuelos.bean.Proveedores;
import org.luisarturopozuelos.db.Conexion;
public class ManejadorDetalleCompras {
    private ArrayList<DetalleCompras> lista = new ArrayList<DetalleCompras>();
    public ManejadorDetalleCompras() {

    }
     public ArrayList<DetalleCompras> getLista() {
        ResultSet datos = Conexion.getInstancia().hacerConsulta("select * from DetalleCompras");
        try{
            while(datos.next()){
               lista.add(new DetalleCompras (datos.getInt("idDetalle"), datos.getInt("idCompra"), datos.getInt("lineaNo"),
                       datos.getInt("idProducto"), datos.getString("descripcion"), datos.getFloat("cantidad"), 
                       datos.getFloat("precio"), datos.getFloat("totalLinea"))); 
            }
            }catch(SQLException e){
            e.printStackTrace();
        }
        return lista;
    }
    
}
