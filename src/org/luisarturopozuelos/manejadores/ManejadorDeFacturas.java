package org.luisarturopozuelos.manejadores;
import java.util.ArrayList;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.luisarturopozuelos.bean.Facturas;
import org.luisarturopozuelos.bean.Proveedores;
import org.luisarturopozuelos.db.Conexion;

public class ManejadorDeFacturas {
    private ArrayList<Facturas> lista = new ArrayList<Facturas>();
    public ManejadorDeFacturas() {

    }
    public ArrayList<Facturas> getLista() {
        ResultSet datos = Conexion.getInstancia().hacerConsulta("select * from vw_vistaFactura");
        try{
            while(datos.next()){
               lista.add(new Facturas(datos.getInt("idFactura"), datos.getInt("numeroDeFactura"), datos.getDate("fecha"),
                       datos.getInt("idCliente"), datos.getString("descripcion"), datos.getFloat("total"))); 
            }
            }catch(SQLException e){
            e.printStackTrace();
        }
        return lista;
    }
}
