package org.luisarturopozuelos.manejadores;
import java.util.ArrayList;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.luisarturopozuelos.bean.Compras;
import org.luisarturopozuelos.bean.DetalleCompras;
import org.luisarturopozuelos.bean.DetalleFacturas;
import org.luisarturopozuelos.bean.DireccionClientes;
import org.luisarturopozuelos.bean.Inventarios;
import org.luisarturopozuelos.bean.DireccionProveedores;
import org.luisarturopozuelos.bean.Proveedores;
import org.luisarturopozuelos.db.Conexion;
public class ManejadorDireccionProveedores {
    
    private ArrayList<DireccionProveedores> lista = new ArrayList<DireccionProveedores>();
    public ManejadorDireccionProveedores (){

    }
     public ArrayList<DireccionProveedores> getLista() {
        ResultSet datos = Conexion.getInstancia().hacerConsulta("sselect * from DireccionProveedores");
        try{
            while(datos.next()){
               lista.add(new DireccionProveedores (datos.getInt("idDireccion"), datos.getInt("idProveedor"), datos.getInt("idTipo"),
                       datos.getString("direccion"))); 
            }
            }catch(SQLException e){
            e.printStackTrace();
        }
        return lista;
    }
}
