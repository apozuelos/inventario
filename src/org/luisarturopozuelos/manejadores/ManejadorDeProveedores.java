package org.luisarturopozuelos.manejadores;
import java.util.ArrayList;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.luisarturopozuelos.bean.Proveedores;
import org.luisarturopozuelos.db.Conexion;
public class ManejadorDeProveedores {
    private ArrayList<Proveedores> lista = new ArrayList<Proveedores>();
    public ManejadorDeProveedores() {

    }
    public ArrayList<Proveedores> getLista() {
        ResultSet datos = Conexion.getInstancia().hacerConsulta("select * from vw_ConsultaProveedores");
        try{
            while(datos.next()){
               lista.add(new Proveedores(datos.getInt("idProveedor"),datos.getString("nit"),datos.getString("nombre"),
               datos.getString("paginaWeb"),datos.getString("contacto"))); 
            }
            }catch(SQLException e){
            e.printStackTrace();
        }
        return lista;
    }
}