package org.luisarturopozuelos.manejadores;
import java.util.ArrayList;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.luisarturopozuelos.bean.Compras;
import org.luisarturopozuelos.bean.DetalleCompras;
import org.luisarturopozuelos.bean.DetalleFacturas;
import org.luisarturopozuelos.bean.DireccionClientes;
import org.luisarturopozuelos.bean.DireccionProveedores;
import org.luisarturopozuelos.bean.EmailClientes;
import org.luisarturopozuelos.bean.EmailProveedores;
import org.luisarturopozuelos.bean.Inventarios;
import org.luisarturopozuelos.bean.Empaques;
import org.luisarturopozuelos.bean.Proveedores;
import org.luisarturopozuelos.db.Conexion;
public class ManejadorEmpaques {
    private ArrayList<Empaques> lista = new ArrayList<Empaques>();
    public ManejadorEmpaques (){

    }
     public ArrayList<Empaques> getLista() {
        ResultSet datos = Conexion.getInstancia().hacerConsulta("select * from vw_Empaques");
        try{
            while(datos.next()){
               lista.add(new Empaques (datos.getInt("idEmpaque"), datos.getString("descripcion"))); 
            }
            }catch(SQLException e){
            e.printStackTrace();
        }
        return lista;
    }
}
