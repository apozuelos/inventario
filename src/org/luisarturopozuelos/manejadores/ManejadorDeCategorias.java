package org.luisarturopozuelos.manejadores;
import java.util.ArrayList;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.luisarturopozuelos.bean.Categorias;
import org.luisarturopozuelos.db.Conexion;

public class ManejadorDeCategorias {
    private ArrayList<Categorias> lista = new ArrayList<Categorias>();
    public ManejadorDeCategorias() {

    }
    public ArrayList<Categorias> getLista() {
        ResultSet datos = Conexion.getInstancia().hacerConsulta("select * from vw_categorias");
        try{
            while(datos.next()){
               lista.add(new Categorias(datos.getString("idCategoria"),datos.getString("descripcion"))); 
            }
            }catch(SQLException e){
            e.printStackTrace();
        }
        return lista;
    }
}
