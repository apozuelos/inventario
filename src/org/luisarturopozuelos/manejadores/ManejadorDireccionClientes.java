package org.luisarturopozuelos.manejadores;
import java.util.ArrayList;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.luisarturopozuelos.bean.Compras;
import org.luisarturopozuelos.bean.DetalleCompras;
import org.luisarturopozuelos.bean.DetalleFacturas;
import org.luisarturopozuelos.bean.Inventarios;
import org.luisarturopozuelos.bean.DireccionClientes;
import org.luisarturopozuelos.bean.Proveedores;
import org.luisarturopozuelos.db.Conexion;
public class ManejadorDireccionClientes {
     private ArrayList<DireccionClientes> lista = new ArrayList<DireccionClientes>();
    public ManejadorDireccionClientes (){

    }
     public ArrayList<DireccionClientes> getLista() {
        ResultSet datos = Conexion.getInstancia().hacerConsulta("select * from DireccionClientes");
        try{
            while(datos.next()){
               lista.add(new DireccionClientes (datos.getInt("idDireccion"), datos.getInt("idCliente"), datos.getInt("idTipo"),
                       datos.getString("direccion"))); 
            }
            }catch(SQLException e){
            e.printStackTrace();
        }
        return lista;
    }
    
}
