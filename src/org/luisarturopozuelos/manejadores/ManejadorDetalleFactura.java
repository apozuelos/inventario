package org.luisarturopozuelos.manejadores;
import java.util.ArrayList;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.luisarturopozuelos.bean.Compras;
import org.luisarturopozuelos.bean.DetalleCompras;
import org.luisarturopozuelos.bean.Inventarios;
import org.luisarturopozuelos.bean.DetalleFacturas;
import org.luisarturopozuelos.bean.Proveedores;
import org.luisarturopozuelos.db.Conexion;
public class ManejadorDetalleFactura {
    
    private ArrayList<DetalleFacturas> lista = new ArrayList<DetalleFacturas>();
    public ManejadorDetalleFactura (){

    }
     public ArrayList<DetalleFacturas> getLista() {
        ResultSet datos = Conexion.getInstancia().hacerConsulta("select * from DetalleFacturas");
        try{
            while(datos.next()){
               lista.add(new DetalleFacturas (datos.getInt("idDetalle"), datos.getInt("idFacturas"), datos.getInt("lineaNo"),
                       datos.getInt("idProducto"), datos.getString("descripcion"), datos.getFloat("cantidad"), 
                       datos.getFloat("precio"), datos.getFloat("totalLinea"))); 
            }
            }catch(SQLException e){
            e.printStackTrace();
        }
        return lista;
    }
}
