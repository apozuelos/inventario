package org.luisarturopozuelos.manejadores;
import java.util.ArrayList;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.luisarturopozuelos.bean.Cliente;
import org.luisarturopozuelos.db.Conexion;
public class ManejadorDeCliente {
    private ArrayList<Cliente> lista = new ArrayList<Cliente>();
    public ManejadorDeCliente() {

    }
    public ArrayList<Cliente> getLista() {
        ResultSet datos = Conexion.getInstancia().hacerConsulta("select * from vw_Clientes");
        try{
            while(datos.next()){
               lista.add(new Cliente(datos.getString("idCliente"),datos.getString("nit"), 
                       datos.getString("nombre"),datos.getString("dpi"))); 
            }
            }catch(SQLException e){
            e.printStackTrace();
        }
        return lista;
    }
}