package org.luisarturopozuelos.manejadores;
import java.util.ArrayList;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.luisarturopozuelos.bean.Compras;
import org.luisarturopozuelos.bean.Facturas;
import org.luisarturopozuelos.bean.Proveedores;
import org.luisarturopozuelos.db.Conexion;

public class ManejadorDeCompras {
    private ArrayList<Compras> lista = new ArrayList<Compras>();
    public ManejadorDeCompras() {

    }
    public ArrayList<Compras> getLista() {
        ResultSet datos = Conexion.getInstancia().hacerConsulta("select * from vw_ConsultaProveedores");
        try{
            while(datos.next()){
               lista.add(new Compras(datos.getInt("idFactura"), datos.getInt("numeroDeCompra"), datos.getDate("fecha"),
                       datos.getInt("idProveedor"), datos.getString("descripcion"), datos.getFloat("total"))); 
            }
            }catch(SQLException e){
            e.printStackTrace();
        }
        return lista;
    }
}
