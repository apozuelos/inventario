package org.luisarturopozuelos.manejadores;
import java.util.ArrayList;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.luisarturopozuelos.bean.Compras;
import org.luisarturopozuelos.bean.DetalleCompras;
import org.luisarturopozuelos.bean.DetalleFacturas;
import org.luisarturopozuelos.bean.DireccionClientes;
import org.luisarturopozuelos.bean.DireccionProveedores;
import org.luisarturopozuelos.bean.EmailClientes;
import org.luisarturopozuelos.bean.Inventarios;
import org.luisarturopozuelos.bean.EmailProveedores;
import org.luisarturopozuelos.bean.Proveedores;
import org.luisarturopozuelos.db.Conexion;
public class ManejadorEmailProveedores {
    private ArrayList<EmailProveedores> lista = new ArrayList<EmailProveedores>();
    public ManejadorEmailProveedores (){

    }
     public ArrayList<EmailProveedores> getLista() {
        ResultSet datos = Conexion.getInstancia().hacerConsulta("select * from EmailProveedores");
        try{
            while(datos.next()){
               lista.add(new EmailProveedores (datos.getInt("idEmail"), datos.getInt("idProveedor"), datos.getInt("idTipo"),
                       datos.getString("email"))); 
            }
            }catch(SQLException e){
            e.printStackTrace();
        }
        return lista;
    }
}
