package org.luisarturopozuelos.manejadores;
import java.util.ArrayList;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.luisarturopozuelos.bean.Categorias;
//import org.luisarturopozuelos.bean.Categorias;
import org.luisarturopozuelos.bean.Productos;
import org.luisarturopozuelos.db.Conexion;
public class ManejadorDeProductos {
    private ArrayList<Productos> lista = new ArrayList<Productos>();
    public ManejadorDeProductos() {

    }
    public ArrayList<Productos> getLista() {
        ResultSet datos = Conexion.getInstancia().hacerConsulta("select * from vw_Consultaproducto");
        try{
            while(datos.next()){
               lista.add(new Productos(datos.getInt("idProducto"),datos.getString("nombre"),datos.getString("descripcion"),
               datos.getFloat("precioUnitario"),datos.getFloat("precioPorDocena"),datos.getFloat("precioPorMayor"),
               datos.getInt("idEmpaque"),datos.getInt("idCategoria"),datos.getInt("idStock"))); 
            }
            }catch(SQLException e){
            e.printStackTrace();
        }
        return lista;
    }
}