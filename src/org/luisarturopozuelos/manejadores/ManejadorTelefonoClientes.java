package org.luisarturopozuelos.manejadores;
import java.util.ArrayList;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.luisarturopozuelos.bean.Compras;
import org.luisarturopozuelos.bean.DetalleCompras;
import org.luisarturopozuelos.bean.DetalleFacturas;
import org.luisarturopozuelos.bean.DireccionClientes;
import org.luisarturopozuelos.bean.DireccionProveedores;
import org.luisarturopozuelos.bean.EmailClientes;
import org.luisarturopozuelos.bean.EmailProveedores;
import org.luisarturopozuelos.bean.Empaques;
import org.luisarturopozuelos.bean.Inventarios;
import org.luisarturopozuelos.bean.Stocks;
import org.luisarturopozuelos.bean.TelefonoClientes;
import org.luisarturopozuelos.db.Conexion;
public class ManejadorTelefonoClientes {
    private ArrayList<TelefonoClientes> lista = new ArrayList<TelefonoClientes>();
    public ManejadorTelefonoClientes (){

    }
     public ArrayList<TelefonoClientes> getLista() {
        ResultSet datos = Conexion.getInstancia().hacerConsulta("select * from vw_TelefonoClientes");
        try{
            while(datos.next()){
               lista.add(new TelefonoClientes (datos.getInt("idTelefono"), datos.getInt("idCliente"), datos.getInt("idTipo"), datos.getInt("telefono"))); 
            }
            }catch(SQLException e){
            e.printStackTrace();
        }
        return lista;
    }
}
