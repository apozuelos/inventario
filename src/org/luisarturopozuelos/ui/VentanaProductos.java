package org.luisarturopozuelos.ui;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import org.luisarturopozuelos.modelo.ModeloProductos;

public class VentanaProductos extends JFrame implements ActionListener {
    
    // atributos
    private JButton btnSaludar;
    private JButton btnSalir;
    private JTable tblDatos;
    private JScrollPane scrDatos;
    private ModeloProductos modelo;
        
    public VentanaProductos() {
        this.setTitle("Ventana Productos");
        this.setSize(1024,900);
        this.setLayout(null);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        btnSaludar = new JButton("Saludar");
        btnSaludar.setBounds(10, 10, 120, 60);
        btnSaludar.addActionListener(this);
        btnSalir = new JButton("Salir");
        btnSalir.setBounds(10, 80, 120, 60);
        btnSalir.addActionListener(this);
         
        // nuevos 
        tblDatos = new JTable();
        modelo = new ModeloProductos();
        tblDatos.setModel(modelo);
        scrDatos = new JScrollPane();
        scrDatos.setBounds(160, 15, 800, 600);
        scrDatos.setViewportView(tblDatos);
       
        this.getContentPane().add(scrDatos);            
        this.getContentPane().add(btnSalir); 
        this.getContentPane().add(btnSaludar);
        this.setVisible(true);        
    }
    
    public void actionPerformed(ActionEvent e) {
          if(e.getSource() == btnSaludar) {
              JOptionPane.showMessageDialog(null, "Hola Bienvenido al paquete swing");
          } else if (e.getSource() == btnSalir) {
              JOptionPane.showMessageDialog(null, "Adios del pauqete swing");
          }          
    }
}