package org.luisarturopozuelos.ui;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import org.luisarturopozuelos.modelo.ModeloCategorias;

public class VentanaCategorias extends JFrame implements ActionListener {
    
    // atributos
    private JButton btnSaludar;
    private JButton btnSalir;
    private JTable tblDatos;
    private JScrollPane scrDatos;
    private ModeloCategorias modelo;
        
    public VentanaCategorias() {
        this.setTitle("Ventana Categorias");
        this.setSize(600,600);
        this.setLayout(null);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        btnSaludar = new JButton("INICIO");
        btnSaludar.setBounds(10, 10, 120, 60);
        btnSaludar.addActionListener(this);
        btnSalir = new JButton("EXIT");
        btnSalir.setBounds(10, 80, 120, 60);
        btnSalir.addActionListener(this);
         
        // nuevos 
        tblDatos = new JTable();
        modelo = new ModeloCategorias();
        tblDatos.setModel(modelo);
        scrDatos = new JScrollPane();
        scrDatos.setBounds(160, 10, 400, 600);
        scrDatos.setViewportView(tblDatos);
       
        this.getContentPane().add(scrDatos);            
        this.getContentPane().add(btnSalir); 
        this.getContentPane().add(btnSaludar);
        this.setVisible(true);        
    }
    
    public void actionPerformed(ActionEvent e) {
          if(e.getSource() == btnSaludar) {
              JOptionPane.showMessageDialog(null, "INICIO DE VENTANA DE CATEGORIAS");
          } else if (e.getSource() == btnSalir) {
              JOptionPane.showMessageDialog(null, "SALIDA DE VENTANA DE CATEGORIA");
          }          
    }
}