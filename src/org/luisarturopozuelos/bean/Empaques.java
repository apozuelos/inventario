package org.luisarturopozuelos.bean;

public class Empaques {
    private int idEmpaque;
    private String descripcion;

    public Empaques(int idEmpaque, String descripcion) {
        this.idEmpaque = idEmpaque;
        this.descripcion = descripcion;
    }

    public int getIdEmpaque() {
        return idEmpaque;
    }

    public void setIdEmpaque(int idEmpaque) {
        this.idEmpaque = idEmpaque;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    
}
