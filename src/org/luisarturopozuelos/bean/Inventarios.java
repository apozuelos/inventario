package org.luisarturopozuelos.bean;
import java.util.Date;

public class Inventarios {
    
    private int idMovimiento;
    private Date fecha;
    private int idProducto;
    private int idTipoMovimiento;
    private int idFactura;
    private int idCompra;
    private int lineaNo;
    private float cantidad;
    private float precio;

    public Inventarios(int idMovimiento, Date fecha, int idProducto, int idTipoMovimiento, int idFactura, int idCompra, int lineaNo, float cantidad, float precio) {
        this.idMovimiento = idMovimiento;
        this.fecha = fecha;
        this.idProducto = idProducto;
        this.idTipoMovimiento = idTipoMovimiento;
        this.idFactura = idFactura;
        this.idCompra = idCompra;
        this.lineaNo = lineaNo;
        this.cantidad = cantidad;
        this.precio = precio;
    }

    public int getIdMovimiento() {
        return idMovimiento;
    }

    public void setIdMovimiento(int idMovimiento) {
        this.idMovimiento = idMovimiento;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public int getIdTipoMovimiento() {
        return idTipoMovimiento;
    }

    public void setIdTipoMovimiento(int idTipoMovimiento) {
        this.idTipoMovimiento = idTipoMovimiento;
    }

    public int getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(int idFactura) {
        this.idFactura = idFactura;
    }

    public int getIdCompra() {
        return idCompra;
    }

    public void setIdCompra(int idCompra) {
        this.idCompra = idCompra;
    }

    public int getLineaNo() {
        return lineaNo;
    }

    public void setLineaNo(int lineaNo) {
        this.lineaNo = lineaNo;
    }

    public float getCantidad() {
        return cantidad;
    }

    public void setCantidad(float cantidad) {
        this.cantidad = cantidad;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    
    
}
