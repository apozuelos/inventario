package org.luisarturopozuelos.bean;
import java.util.Date;
public class Compras {
    
    private int idCompra;
    private int numeroDeCompra;
    private Date fecha;
    private int idProveedor;
    private String descripcion;
    private float total;

    public Compras(int idCompra, int numeroDeCompra, Date fecha, int idProveedor, String descripcion, float total) {
        this.idCompra = idCompra;
        this.numeroDeCompra = numeroDeCompra;
        this.fecha = fecha;
        this.idProveedor = idProveedor;
        this.descripcion = descripcion;
        this.total = total;
    }

    public int getIdCompra() {
        return idCompra;
    }

    public void setIdCompra(int idCompra) {
        this.idCompra = idCompra;
    }

    public int getNumeroDeCompra() {
        return numeroDeCompra;
    }

    public void setNumeroDeCompra(int numeroDeCompra) {
        this.numeroDeCompra = numeroDeCompra;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public int getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(int idProveedor) {
        this.idProveedor = idProveedor;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }
    
    
}
