package org.luisarturopozuelos.bean;
import java.util.Date;

public class Facturas {
    private int idFactura;
    private int numeroDeFactura;
    private Date fecha;
    private int idCliente;
    private String descripcion;
    private float total;

    public Facturas(int idFactura, int numeroDeFactura, Date fecha, int idCliente, String descripcion, float total) {
        this.idFactura = idFactura;
        this.numeroDeFactura = numeroDeFactura;
        this.fecha = fecha;
        this.idCliente = idCliente;
        this.descripcion = descripcion;
        this.total = total;
    }

    public int getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(int idFactura) {
        this.idFactura = idFactura;
    }

    public int getNumeroDeFactura() {
        return numeroDeFactura;
    }

    public void setNumeroDeFactura(int numeroDeFactura) {
        this.numeroDeFactura = numeroDeFactura;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    

    
}
