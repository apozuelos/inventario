package org.luisarturopozuelos.bean;

public class DetalleFacturas {
    
    private int idDetalle;
    private int idFactura;
    private int lineaNo;
    private int idProducto;
    private String descripcion;
    private float cantidad;
    private float precio;
    private float totalLinea;

    public DetalleFacturas(int idDetalle, int idFactura, int lineaNo, int idProducto, String descripcion, float cantidad, float precio, float totalLinea) {
        this.idDetalle = idDetalle;
        this.idFactura = idFactura;
        this.lineaNo = lineaNo;
        this.idProducto = idProducto;
        this.descripcion = descripcion;
        this.cantidad = cantidad;
        this.precio = precio;
        this.totalLinea = totalLinea;
    }

    public int getIdDetalle() {
        return idDetalle;
    }

    public void setIdDetalle(int idDetalle) {
        this.idDetalle = idDetalle;
    }

    public int getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(int idFactura) {
        this.idFactura = idFactura;
    }

    public int getLineaNo() {
        return lineaNo;
    }

    public void setLineaNo(int lineaNo) {
        this.lineaNo = lineaNo;
    }

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public float getCantidad() {
        return cantidad;
    }

    public void setCantidad(float cantidad) {
        this.cantidad = cantidad;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public float getTotalLinea() {
        return totalLinea;
    }

    public void setTotalLinea(float totalLinea) {
        this.totalLinea = totalLinea;
    }
    
    
}
