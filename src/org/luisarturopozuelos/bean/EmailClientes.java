package org.luisarturopozuelos.bean;

public class EmailClientes {
    
    private int idEmail;
    private int idCliente;
    private int idTipo;
    private String email;

    public EmailClientes(int idEmail, int idCliente, int idTipo, String email) {
        this.idEmail = idEmail;
        this.idCliente = idCliente;
        this.idTipo = idTipo;
        this.email = email;
    }

    public int getIdEmail() {
        return idEmail;
    }

    public void setIdEmail(int idEmail) {
        this.idEmail = idEmail;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public int getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(int idTipo) {
        this.idTipo = idTipo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    
    
}
