package org.luisarturopozuelos.bean;

public class TelefonoClientes {
    
    private int idTelefono;
    private int idCliente;
    private int idTipo;
    private int telefono;

    public TelefonoClientes(int idTelefono, int idCliente, int idTipo, int telefono) {
        this.idTelefono = idTelefono;
        this.idCliente = idCliente;
        this.idTipo = idTipo;
        this.telefono = telefono;
    }

    public int getIdTelefono() {
        return idTelefono;
    }

    public void setIdTelefono(int idTelefono) {
        this.idTelefono = idTelefono;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public int getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(int idTipo) {
        this.idTipo = idTipo;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }
    
    
}
