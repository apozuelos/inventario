package org.luisarturopozuelos.bean;

public class TelefonoProveedores {
    private int idTelefono;
    private int idProveedores;
    private int idTipo;
    private int telefono;

    public TelefonoProveedores(int idTelefono, int idProveedores, int idTipo, int telefono) {
        this.idTelefono = idTelefono;
        this.idProveedores = idProveedores;
        this.idTipo = idTipo;
        this.telefono = telefono;
    }

    public int getIdTelefono() {
        return idTelefono;
    }

    public void setIdTelefono(int idTelefono) {
        this.idTelefono = idTelefono;
    }

    public int getIdProveedores() {
        return idProveedores;
    }

    public void setIdProveedores(int idProveedores) {
        this.idProveedores = idProveedores;
    }

    public int getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(int idTipo) {
        this.idTipo = idTipo;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }
    
    
}
