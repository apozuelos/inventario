package org.luisarturopozuelos.bean;

public class Stocks {
    
    private int idStock;
    private float stock;

    public Stocks(int idStock, float stock) {
        this.idStock = idStock;
        this.stock = stock;
    }

    public int getIdStock() {
        return idStock;
    }

    public void setIdStock(int idStock) {
        this.idStock = idStock;
    }

    public float getStock() {
        return stock;
    }

    public void setStock(float stock) {
        this.stock = stock;
    }
    
    
}
