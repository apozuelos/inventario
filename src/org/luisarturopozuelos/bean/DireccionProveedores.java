package org.luisarturopozuelos.bean;

public class DireccionProveedores {
    private int idDireccion;
    private int idProveedor;
    private int idTipo;
    private String direccion;

    public DireccionProveedores(int idDireccion, int idProveedor, int idTipo, String direccion) {
        this.idDireccion = idDireccion;
        this.idProveedor = idProveedor;
        this.idTipo = idTipo;
        this.direccion = direccion;
    }

    public int getIdDireccion() {
        return idDireccion;
    }

    public void setIdDireccion(int idDireccion) {
        this.idDireccion = idDireccion;
    }

    public int getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(int idProveedor) {
        this.idProveedor = idProveedor;
    }

    public int getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(int idTipo) {
        this.idTipo = idTipo;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    
    
}
