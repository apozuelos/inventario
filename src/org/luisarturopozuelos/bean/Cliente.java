package org.luisarturopozuelos.bean;
public class Cliente {
    private String idCliente;
    private String nit;
    private String nombre;
    private String dpi;
    
    public Cliente() {
    }
    public Cliente(String idCliente, String nit, String nombre, String dpi) {
        this.idCliente = idCliente;
        this.nit = nit;
        this.nombre = nombre;
        this.dpi = dpi;
        
    }
    public String getIdCliente(){
        return idCliente;
    }
    public void setIdCliente(String idCliente){
        this.idCliente = idCliente;
    }
    public String getNit() {
        return nit;
    }
    public void setNit(String nit) {
        this.nit = nit;
    }
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getDpi() {
        return dpi;
    }
    public void setDpi(String dpi) {
        this.dpi = dpi;
    }
   
}
