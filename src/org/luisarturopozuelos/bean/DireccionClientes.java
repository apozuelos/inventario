package org.luisarturopozuelos.bean;

public class DireccionClientes {
    
    private int idDireccion;
    private int idClientes;
    private int idTipo;
    private String direccion;

    public DireccionClientes(int idDireccion, int idClientes, int idTipo, String direccion) {
        this.idDireccion = idDireccion;
        this.idClientes = idClientes;
        this.idTipo = idTipo;
        this.direccion = direccion;
    }

    public int getIdDireccion() {
        return idDireccion;
    }

    public void setIdDireccion(int idDireccion) {
        this.idDireccion = idDireccion;
    }

    public int getIdClientes() {
        return idClientes;
    }

    public void setIdClientes(int idClientes) {
        this.idClientes = idClientes;
    }

    public int getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(int idTipo) {
        this.idTipo = idTipo;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    
    
}
