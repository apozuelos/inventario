package org.luisarturopozuelos.bean;

public class Productos {
    private int idProducto;
    private String nombre;
    private String descripcion;
    private float precioUnitario;
    private float precioPorDocena;
    private float precioPorMayor;
    private int idEmpaque;
    private int idCategoria;
    private int idStock;

    public Productos(int idProducto, String nombre, String descripcion, float precioUnitario, float precioPorDocena, float precioporMayor, int idEmpaque, int idCategoria, int idStock) {
        this.idProducto = idProducto;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.precioUnitario = precioUnitario;
        this.precioPorDocena = precioPorDocena;
        this.precioPorMayor = precioPorMayor;
        this.idEmpaque = idEmpaque;
        this.idCategoria = idCategoria;
        this.idStock = idStock;
    }

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public float getPrecioUnitario() {
        return precioUnitario;
    }

    public void setPrecioUnitario(float precioUnitario) {
        this.precioUnitario = precioUnitario;
    }

    public float getPrecioPorDocena() {
        return precioPorDocena;
    }

    public void setPrecioPorDocena(float precioPorDocena) {
        this.precioPorDocena = precioPorDocena;
    }

    public float getPrecioPorMayor() {
        return precioPorMayor;
    }

    public void setPrecioporMayor(float precioporMayor) {
        this.precioPorMayor = precioporMayor;
    }

    public int getIdEmpaque() {
        return idEmpaque;
    }

    public void setIdEmpaque(int idEmpaque) {
        this.idEmpaque = idEmpaque;
    }

    public int getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }

    public int getIdStock() {
        return idStock;
    }

    public void setIdStock(int idStock) {
        this.idStock = idStock;
    }

    
    
    
    
    
    
    
}