package org.luisarturopozuelos.bean;

public class EmailProveedores {
    
    private int idEmail;
    private int idProveedor;
    private int idTipo;
    private String email;

    public EmailProveedores(int idEmail, int idProveedor, int idTipo, String email) {
        this.idEmail = idEmail;
        this.idProveedor = idProveedor;
        this.idTipo = idTipo;
        this.email = email;
    }

    public int getIdEmail() {
        return idEmail;
    }

    public void setIdEmail(int idEmail) {
        this.idEmail = idEmail;
    }

    public int getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(int idProveedor) {
        this.idProveedor = idProveedor;
    }

    public int getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(int idTipo) {
        this.idTipo = idTipo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    
}
