package org.luisarturopozuelos.modelo;
import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import org.luisarturopozuelos.manejadores.ManejadorDeCategorias;
import org.luisarturopozuelos.bean.Categorias;

public class ModeloCategorias extends AbstractTableModel {
    
    private String [] encabezados =  {"idCategoria", "descripcion"};    
    private ArrayList<Categorias> listaCategorias = null;
    private ManejadorDeCategorias manejador = new ManejadorDeCategorias();
    
    public ModeloCategorias() {
        listaCategorias = manejador.getLista();        
    }
       
    public String getColumnName(int columna) {
        return encabezados[columna];
    }
    public int getColumnCount() {
        return encabezados.length;
    }
    public int getRowCount() {
        return listaCategorias.size();
    }
    public Object getValueAt(int fila, int columna) {
        String resultado = "";
        Categorias elemento = listaCategorias.get(fila);
        switch(columna) {
            case 0:
                     resultado  = String.valueOf(elemento.getIdCategoria());
                    break;
            case 1:
                    resultado = elemento.getDescripcion();
                    break;
            
        }
        return resultado;
    }
}
