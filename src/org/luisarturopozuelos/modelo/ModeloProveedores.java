package org.luisarturopozuelos.modelo;
import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import org.luisarturopozuelos.manejadores.ManejadorDeProveedores;
import org.luisarturopozuelos.bean.Proveedores;

public class ModeloProveedores extends AbstractTableModel {
    
    private String [] encabezados =  {"idProveedor", "nit", "nombre", "paginaWeb", "contacto"};    
    private ArrayList<Proveedores> listaProveedores = null;
    private ManejadorDeProveedores manejador = new ManejadorDeProveedores();
    
    public ModeloProveedores() {
        listaProveedores = manejador.getLista();        
    }
       
    public String getColumnName(int columna) {
        return encabezados[columna];
    }
    public int getColumnCount() {
        return encabezados.length;
    }
    public int getRowCount() {
        return listaProveedores.size();
    }
    public Object getValueAt(int fila, int columna) {
        String resultado = "";
        Proveedores elemento = listaProveedores.get(fila);
        switch(columna) {
            case 0:
                     resultado  = String.valueOf(elemento.getIdProveedor());
                    break;
            case 1:
                    resultado = String.valueOf(elemento.getNit());
                    break;
            case 2:
                    resultado = elemento.getNombre();
                    break;
            case 3:
                    resultado = elemento.getPaginaWeb();
                    break;
            case 4:
                    resultado = elemento.getContacto();
                    break;
            
        }
        return resultado;
    }
}
