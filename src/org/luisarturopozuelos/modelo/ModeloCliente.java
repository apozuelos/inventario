package org.luisarturopozuelos.modelo;
import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import org.luisarturopozuelos.manejadores.ManejadorDeCliente;
import org.luisarturopozuelos.bean.Cliente;

public class ModeloCliente extends AbstractTableModel {
    
    private String [] encabezados =  {"idCliente", "nit", "nombre", "dpi"};    
    private ArrayList<Cliente> listaClientes = null;
    private ManejadorDeCliente manejador = new ManejadorDeCliente();
    
    public ModeloCliente() {
        listaClientes = manejador.getLista();        
    }
    
    public String getColumnName(int columna) {
        return encabezados[columna];
    }
    public int getColumnCount() {
        return encabezados.length;
    }
    public int getRowCount() {
        return listaClientes.size();
    }
    public Object getValueAt(int fila, int columna) {
        String resultado = "";
        Cliente elemento = listaClientes.get(fila);
        switch(columna) {
            case 0:
                     resultado  = String.valueOf(elemento.getIdCliente());
                    break;
            case 1:
                    resultado = elemento.getNit();
                    break;
            case 2:
                    resultado = elemento.getNombre();
                    break;
            case 3:
                    resultado = elemento.getDpi();
                    break;
            
        }
        return resultado;
    }
}
