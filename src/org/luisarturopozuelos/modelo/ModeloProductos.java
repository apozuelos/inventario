package org.luisarturopozuelos.modelo;
import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import org.luisarturopozuelos.manejadores.ManejadorDeProductos;
import org.luisarturopozuelos.bean.Productos;

public class ModeloProductos extends AbstractTableModel {
    
    private String [] encabezados =  {"idProducto", "nombre", "descripcion", "precioUnitario", "precioPorDocena",
    "precioPorMayor", "idEmpaque", "idCategoria", "idStock"};    
    private ArrayList<Productos> listaProductos = null;
    private ManejadorDeProductos manejador = new ManejadorDeProductos();
    
    public ModeloProductos() {
        listaProductos = manejador.getLista();        
    }
       
    public String getColumnName(int columna) {
        return encabezados[columna];
    }
    public int getColumnCount() {
        return encabezados.length;
    }
    public int getRowCount() {
        return listaProductos.size();
    }
    public Object getValueAt(int fila, int columna) {
        String resultado = "";
        Productos elemento = listaProductos.get(fila);
        switch(columna) {
            case 0:
                     resultado  = String.valueOf(elemento.getIdProducto());
                    break;
            case 1:
                    resultado = elemento.getNombre();
                    break;
            case 2:
                    resultado = elemento.getDescripcion();
                    break;
            case 3:
                    resultado = String.valueOf(elemento.getPrecioUnitario());
                    break;
            case 4:
                    resultado = String.valueOf(elemento.getPrecioPorDocena());
                    break;
            case 5:
                    resultado = String.valueOf(elemento.getPrecioPorMayor());
                    break;
            case 6:
                    resultado = String.valueOf(elemento.getIdEmpaque());
                    break;
            case 7:
                    resultado = String.valueOf(elemento.getIdCategoria());
                    break;
            case 8:
                    resultado = String.valueOf(elemento.getIdStock());
                    break;
        }
        return resultado;
    }
}
