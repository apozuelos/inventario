package org.luisarturopozuelos.db;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.DriverManager;
import java.sql.Statement;
import com.microsoft.sqlserver.jdbc.SQLServerDriver;
public class Conexion {
    private Connection conexion;
    private Statement sentencia;
    private static Conexion instancia;
    public static Conexion getInstancia(){
        if(instancia == null){
            instancia = new Conexion();
        }
        return instancia;
    }
    public Conexion() {
        try{
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
            conexion = DriverManager.getConnection("jdbc:sqlserver://HP-N24:0;"
                    + "instanceName=SQLEXPRESS;dataBaseName=Inventario;user=sa;"
                    + "password=sa;");
            sentencia = conexion.createStatement();
        }catch(ClassNotFoundException e){
            e.printStackTrace();
        }catch(InstantiationException e){
            e.printStackTrace();
        }catch(IllegalAccessException e){
            e.printStackTrace();
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
    public ResultSet hacerConsulta(String consulta){
        ResultSet resultado = null;
        try{
            resultado = sentencia.executeQuery(consulta);
        }catch(SQLException e){
            e.printStackTrace();
        }
        return resultado;
    }
}