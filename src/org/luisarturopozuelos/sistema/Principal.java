package org.luisarturopozuelos.sistema;
import org.luisarturopozuelos.bean.Cliente;
import org.luisarturopozuelos.bean.Categorias;
import org.luisarturopozuelos.bean.Productos;
import org.luisarturopozuelos.bean.Proveedores;
import org.luisarturopozuelos.manejadores.ManejadorDeCliente;
import org.luisarturopozuelos.manejadores.ManejadorDeCategorias;
import org.luisarturopozuelos.manejadores.ManejadorDeProductos;
import org.luisarturopozuelos.manejadores.ManejadorDeProveedores;
import org.luisarturopozuelos.ui.VentanaCliente;
import org.luisarturopozuelos.ui.VentanaProductos;
import org.luisarturopozuelos.ui.VentanaCategorias;
import org.luisarturopozuelos.ui.VentanaProveedores;
public class Principal {
    public static void main(String args[]){
            ManejadorDeCliente manejador = new ManejadorDeCliente();
            System.out.println("Clientes");
            for(Cliente elemento : manejador.getLista()){
                System.out.println("Nit: "+ elemento.getNit() + "Nombre: " + elemento.getNombre() + "Dpi: " + elemento.getDpi());
            }
            ManejadorDeCategorias manejador2 = new ManejadorDeCategorias();
            System.out.println("Categorias");
            for(Categorias elemento : manejador2.getLista()){
                System.out.println("Id Categorias:  "+ elemento.getIdCategoria() + "  Descripcion del Producto: " + elemento.getDescripcion());
            }
            ManejadorDeProductos manejador3 = new ManejadorDeProductos();
            System.out.println("Productos");
            for(Productos elemento : manejador3.getLista()){
                System.out.println("Nombre del Producto: " + elemento.getNombre() + "Descripcion del Producto: " + elemento.getDescripcion() + "Precio Unitario: " + elemento.getPrecioUnitario());
            }
            ManejadorDeProveedores manejador4 = new ManejadorDeProveedores();
            System.out.println("Proveedores");
            for(Proveedores elemento : manejador4.getLista()){
                System.out.println("Nombre de la Empresa: " + elemento.getNombre() + " nit: " + elemento.getNit() + " Pagina Web: " + elemento.getPaginaWeb()
                + " Contacto: " + elemento.getContacto());
            }
            VentanaCliente ventanaCliente = new VentanaCliente();
            VentanaProductos ventanaProducto = new VentanaProductos();
            VentanaCategorias ventanaCategoria = new VentanaCategorias();
            VentanaProveedores ventanaProveedore = new VentanaProveedores();
    }
}